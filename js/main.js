var contador_seccion = 1;
var func = [];
var secciones;

window.onload = init;

function init()
{
	secciones = document.getElementsByClassName("seccion");
	setTimeout(mostrar,2000);
	asignarEventos();
}
function reproducirSonido(event)
{
	document.getElementById("audio_"+this.i).play();
}
function asignarEventos(){
	
	var btns = document.getElementsByClassName("boton");

	for(var i=0;i<btns.length;i++)
	{	
		btns[i].addEventListener("click",reproducirSonido.bind({i:i+1}));
	}
}

function ocultarSecciones(){
	for(var i=0;i<secciones.length;i++)
	{
		secciones[i].className += " hidden";
	}
}

function mostrar()
{
	ocultarSecciones();
	console.log(secciones);
	secciones[contador_seccion].className = "animated fadeInUp";
}